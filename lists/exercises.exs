defmodule Excercise do
  def mapsum(l, func), do: mapsum(l, func, 0)

  def mapsum([], _func, total), do: total
  def mapsum([h | t], func, total) do
    val = func.(h)
    mapsum(t, func, total + val)
  end

  def max2([]), do: []
  def max2([h | t]), do: max2(t, h)

  defp max2([], cur_max), do: IO.puts "Current max #{cur_max}"
  defp max2([h | t], cur_max) when h > cur_max, do: max2(t, h)
  defp max2([h | t], cur_max) when h <= cur_max, do: max2(t, cur_max)

  # a - 67, z - 122
  def ceasar([], _n), do: []
  def ceasar(l, n), do: ceasar(l, n, [])

  defp ceasar([], _n, res), do: Enum.reverse(res)
  defp ceasar([h|t], n, res) when h + n > 122 do
    ceasar(t, n, [over_point(h, n) | res])
   end
  defp ceasar([h |t], n, res), do: ceasar(t, n, [h + n| res])

  defp over_point(h, n), do: ((h + n) - 122) + 96

end


defmodule MyList do
  def square([]), do: []
  def square([head | tail]), do: [head * head | square(tail)]

  def map([], _func), do: []
  def map([ head | tail ], func), do: [ func.(head) | map(tail, func) ]

  def sum(l) when is_number(l), do: l
  def sum(l), do: _sum(l, 0) 
  defp _sum([], total), do: total
  defp _sum([head | tail], total), do: _sum(tail, head + total)

  def sum2([]), do: 0
  def sum2([h|t]), do: h + sum2(t)

  def reduce([], value, _), do: value
  def reduce([h|t], value, func), do: reduce(t, func.(h, value), func)

  def span(from, to) when from == to or from > to, do: [from]
  def span(from, to) when from < to, do: [ from | span(from+1, to) ]
end

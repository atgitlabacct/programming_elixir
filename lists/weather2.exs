defmodule WeatherHistory do

  def for_location([], _loc_id), do: []

  def for_location([ [_time, loc_id, _temp, _rain] | tail ], loc_id) do
    [ [_time, loc_id, _temp, _rain] | for_location(tail, loc_id) ]
  end

  def for_location([_ | tail ], loc_id), do: for_location(tail, loc_id)

end

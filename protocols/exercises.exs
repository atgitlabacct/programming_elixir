defmodule ProgElixir.Protocols.Exercises do
  defprotocol Caesar do
    def encrypt(string, shift)
    def rot13(string)
  end

  defimpl Caesar, for: [List, BitString] do
    def encrypt(string, shift) do
      Enum.map(to_char_list(string), fn(c) ->
        case c do
          122 -> 97 + (shift - 1)
          90 -> 65 + (shift - 1)
          _ -> c + shift
        end
      end)
    end

    def rot13(string) do
      encrypt(string, 13)
    end
  end
end

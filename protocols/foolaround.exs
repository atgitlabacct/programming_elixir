defmodule MyActions do
  defimpl ProgElixir.Protocols.FoolAround.Person.Actions, for: Map do
    def print(arg) do
      IO.puts "Generic Map"
    end
  end

  defimpl ProgElixir.Protocols.FoolAround.Person.Actions, for: ProgElixir.Protocols.FoolAround.Person do
    def print(arg) do
      IO.puts "Person"
    end
  end
end

defmodule ProgElixir.Protocols.FoolAround.Person do
  defprotocol Actions do
    def print(arg)
  end

  defstruct [:name, :age, :sex]
end



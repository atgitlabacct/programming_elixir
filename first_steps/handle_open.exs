handle_open = fn
  {:ok, file} -> "First line: #{IO.read(file, :line)}"
  {_, error} -> "Error: #{:file.format_error(error)}"
end
{:ok, path} = File.cwd
IO.puts path

IO.puts handle_open.(File.open("Rakefile"))
IO.puts handle_open.(File.open("nonexistent"))

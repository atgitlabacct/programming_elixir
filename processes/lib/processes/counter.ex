defmodule Processes.Counter do
  use GenServer

  ############################################################
  ## Client Api
  ############################################################

  @doc """
  Start the Process and link to the caller. Keyword of :counter sets 
  the starting counter.
  """
  @type arguments :: [{:counter, integer}]
  @spec start_link(arguments, Genserver.options) :: GenServer.on_start
  def start_link(args, opts) do
    GenServer.start_link( __MODULE__, Keyword.get(args, :counter), opts)
  end

  @doc """
  Increment the counter
  """
  @spec increment(pid) :: any
  def increment(pid) do
    GenServer.call(pid, :increment)
  end

  @doc """
  Stop the process using cast - async.

  Note that if this is supervised, calling stop will make the process
  restart.  In order to really stop a process if it is being supervised
  you need to to call Supervisor.terminate_child

  When supervising(not trapping)
  Test reason   | Outcome
  :kill         | Received error, but supervisor restarted
                | Did not execute see terminate message get called
                | Did not see the next init message on restart
                | Supervisor did restart
  :normal       | No error
                | terminate and init callback where applied
  :shutdown     | Same as :normal
  (other atom)  | Same as :kill (non graceful??)
  """
  def stop(pid, reason) do
    GenServer.cast pid, {:stop, reason}
  end

  def cause_error(pid) do
    GenServer.cast pid, :cause_error
  end

  ############################################################
  ## Server Api
  ############################################################

  def init(counter) do
    Process.flag(:trap_exit, true)
    IO.puts "Starting with #{counter} in pid #{inspect self}"
    {:ok, counter}
  end

  def handle_call(:increment, _from, state) do
    {:reply, state + 1, state + 1}
  end

  def handle_cast({:stop, reason}, state) do
    {:stop, reason, state}
  end

  def handle_cast(:cause_error, state) do
    exit :boom
    {:noreply, state}
  end

  def handle_info(_msg, state) do
    IO.puts "A message came in"
    {:noreply, state}
  end

  def terminate(_reason, _state) do
    IO.puts "Terminating #{inspect self}"
  end
end

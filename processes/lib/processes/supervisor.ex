defmodule Processes.Supervisor do
  use Supervisor

  ############################################################
  ## Client Api
  ############################################################

  def start_link do
    Supervisor.start_link(__MODULE__, [], [])
  end

  @spec add_child(pid, atom, integer) :: Supervisor.on_start_child
  def add_child(sup, name, counter) do
    opts = [name: name, timeout: 5000, debug: [{:log_to_file, "/tmp/test.log"}]]
    args = [counter: counter]
    Supervisor.start_child(sup, [args, opts])
  end

  def light_it_up do
    {:ok, sup} = __MODULE__.start_link
    # {:ok, first} = __MODULE__.add_child(sup, :first, 100)
    # {:ok, second} =__MODULE__.add_child(sup, :second, 200)
    # {:ok, third} =__MODULE__.add_child(sup, :third, 300)
    {:ok, fourth} =__MODULE__.add_child(sup, :fourth, 400)
    # Processes.Counter.stop first, :normal
    # Processes.Counter.stop second, :kill
    # Processes.Counter.stop third, :boom
    Process.unlink sup
    Processes.Counter.cause_error fourth
    sup
  end
  ############################################################
  ## Server Api
  ############################################################

  def init([]) do
    children = [ worker(Processes.Counter, []) ]
    opts = [strategy: :simple_one_for_one, name: __MODULE__]
    supervise children, opts
  end

end

defmodule Issues.GithubIssues do
  require Logger

  @user_agent [ {"User-agent", "Elixir adam.trepanier@gmail.com"} ]
  @github_url Application.get_env(:issues, :github_url)

  def fetch(user, project) do
    Logger.info("URL: #{issues_url(user, project)}")
    Logger.info("Fetching user #{user}'s project #{project}'")
    issues_url(user, project) 
    |> HTTPoison.get(@user_agent)
    |> handle_response
  end

  def issues_url(user, project) do
    "#{@github_url}/repos/#{user}/#{project}/issues"
  end
  
  def handle_response({:ok, %HTTPoison.Response{status_code: 200, body: body}}) do 
    Logger.info("Successful response")
    { :ok, :jsx.decode(body) }
  end

  def handle_response({:ok, %HTTPoison.Response{status_code: status_code, body: body}}) do 
    Logger.info("Error #{status_code} returned")
    Logger.debug fn -> inspect(body) end
    { :error, :jsx.decode(body) }
  end

  def handle_response({:error, %HTTPoison.Error{id: id, reason: reason}}) do 
    Logger.error("Error #{reason} returned")
    { :error, reason }
  end

end

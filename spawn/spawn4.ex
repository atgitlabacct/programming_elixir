# Lets make this greeter actually keep greeting instead of just
# being unfriendly and greeting only one person (message).
defmodule Spawn2 do
  def greet do
    receive do
      {sender, msg} ->
        send sender, {:ok, "hello #{msg}"}
        greet
    end
  end
end

pid = spawn(Spawn2, :greet, [])
send pid, {self, "World!"}
receive do
  {:ok, msg} ->
    IO.puts msg
end

Enum.each(1..10, fn(i) ->
  send pid, {self, "#{i}"}
  # Here we timeout
  receive do
    {:ok, msg} ->
      IO.puts msg
    after 1000 ->
      IO.puts "AHHH, the greeter is gonzo"
    end
end)

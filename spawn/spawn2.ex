defmodule Spawn2 do
  def greet do
    receive do
      {sender, msg} ->
        send sender, {:ok, "hello #{msg}"}
    end
  end
end

pid = spawn(Spawn2, :greet, [])
send pid, {self, "World!"}
receive do
  {:ok, msg} ->
    IO.puts msg
end

send pid, {self, "Waldo"}
# This will never get ran because the Spawn2 pid is no longer
# looking for a message.  The receive block is not recursing or
# we would have to have an 'after' in the receive below to timeout.
#
# Spawn3 does that exact thing -- after
receive do
  {:ok, msg} ->
    IO.puts msg
end

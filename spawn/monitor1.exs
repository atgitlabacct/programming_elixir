# Monitors are used more so for exactly what it says; to
# monitor another process. spawn_link is where two process
# are linked and if one dies so does the other.
#
# spawn_monitor actually returns a tuple {Pid, Ref} where
# the Pid is the spawn process (sad_method) and Ref is
# the actual monitor.
defmodule Monitor1 do
  import :timer, only: [ sleep: 1 ]

  def mon_spawn(sad_fun_atom), do: spawn(Monitor1, :run, [sad_fun_atom])

  def run(sad_fun_atom) do
    res = spawn_monitor(__MODULE__, sad_fun_atom, [])
    IO.puts "Spawned monitor #{inspect res}"

    receive do
      msg  ->
        IO.puts "MESSAGE RECEIVED: #{inspect msg}"
    after 1000 ->
      IO.puts "Nothing happened"
    end
  end

  def sad_method_exit do
    sleep 500
    exit(:boom)
  end

  def sad_method_raise do
    sleep 500
    raise("boom")
  end

end

# With spawn_monitor and an exit :boom a msg will be recieved.
# Spawned monitor {#PID<0.398.0>, #Reference<0.0.0.1896>}
# MESSAGE RECEIVED: {:DOWN, #Reference<0.0.0.1896>, :process, #PID<0.398.0>, :boom}
Monitor1.run(:sad_method_exit)

# When running this an error is raised in the child process and a msg is
# received in the parent.
# MESSAGE RECEIVED: {:DOWN, #Reference<0.0.0.2134>, :process, #PID<0.432.0>,
#   {%RuntimeError{message: "boom"}, [{Monitor1, :sad_method_raise, 0, 
#      [file: 'spawn/monitor1.exs', line: 32]}]}}
# Monitor1.run(:sad_method_raise)


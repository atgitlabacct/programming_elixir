defmodule Link3 do
  import :timer, only: [ sleep: 1 ]

  def run_link(trap, sad_fun_atom) do
    Process.flag(:trap_exit, trap)
    spawn_link(__MODULE__, sad_fun_atom, [])
    receive do
      msg ->
        IO.puts "MESSAGE RECEIVED: #{inspect msg}"
    after 1000 ->
      IO.puts "Nothing happened as far as I am concerned"
    end
  end

  def sad_function_exit do
    sleep 500
    exit(:boom)
  end

  def sad_function_raise do
    sleep 500
    raise("boom")
  end

end

# With the Process.flag(:trap_exit, true) and an exit(:boom) the child process
# will exit and the link will receive the message before it exits
# MESSAGE RECEIVED: {:EXIT, #PID<0.349.0>, :boom}
# Link3.run_link(true, :sad_function_exit)

# With the Process.flag(:trap_exit, false) and an exit(:boom) we will get
# an exit :boom and the link will NOT receive msg
# == Compilation error on file spawn/link3.exs ==
# ** (exit) :boom
# ** (exit) shutdown: 1
# Link3.run_link(false, :sad_function_exit)

# With the Process.flag(:trap_exit, true) and a raise "boom" we will get
# an exception in the child process, but the message is received by the parent 
# with the tuple containing a RuntimeError Struct and the message: boom
# {:EXIT, #PID<0.317.0>, {%RuntimeError{message: "boom"}, [{Link3, 
#        :sad_function_raise, 0, [file: 'spawn/link3.exs', line: 22]}]}}
# Link3.run_link(true, :sad_function_raise)

# With the Process.flag(:trap_exit, false) and a raise "boom" we will get
# an exception in the child process and the message is NOT received.  
# We simply get a runtime error.
# ** (exit) an exception was raised:
# ** (RuntimeError) boom
#    spawn/link3.exs:22: Link3.sad_function_raise/0
# ** (exit) shutdown: 1
# Link3.run_link(false, :sad_function_raise)

defmodule Chain do
  def counter(next_pid) do
    receive do
      {from, n}  ->
        # IO.puts "Received #{n} from:"
        # IO.inspect from
        send next_pid, {self, n + 1}
    end
  end

  @doc """
  Given n it generates n processes that will use the previous generated
  pid to send to.  It also starts the first process with the current pid (self)
  To see this in action uncomment the IO.inspect calls
  """
  def create_processes(n) do
    last = Enum.reduce(1..n, self, fn (_, send_to) ->
      pid = spawn(Chain, :counter, [send_to])
      # IO.inspect pid
      # IO.inspect send_to  # will be previous pid
      pid
    end)

    # send a starting number
    send last, {self, 10}

    # Receive the message from the first started pid
    receive do
      {from, final_answer} when is_integer(final_answer) ->
        # IO.puts "First process spawned was:"
        # IO.inspect from
        "Result is #{inspect(final_answer)}"
    end
  end

  def run(n) do
    IO.puts inspect :timer.tc(Chain, :create_processes, [n])
  end
end

defmodule Sequence.Supervisor do
  use Supervisor

  def start_link(number) do
    result = {:ok, main_sup} = Supervisor.start_link(__MODULE__, [], name: :main_supervisor)
    start_workers(main_sup, number)
    result
  end

  def start_workers(sup, number) do
    # Start the stash worker
    # we add the workers to the supervisor (sup) via the start_child
    {:ok, stash} = Supervisor.start_child(sup, worker(Sequence.Stash, [number]))
    # Now start the sub-supervisor for the actual Sequence.Server
    # while main the main supervisor supervise the sub-supervisor
    # and we pass in the stash pid to the sub-supervisor.
    Supervisor.start_child(sup, supervisor(Sequence.SubSupervisor, [stash]))
  end

  def init(_) do
    supervise [], strategy: :one_for_one
  end
end

defmodule Sequence.SubSupervisor do
  use Supervisor

  ############################################################
  ## Client Api
  ############################################################
  
  def start_link(stash_pid) do
    {:ok, pid} = Supervisor.start_link(__MODULE__, stash_pid)
  end

  ############################################################
  ## Server Api
  ############################################################
  
  def init(stash_pid) do
    IO.puts "Intializing supsupervisor"
    child_processes = [ worker(Sequence.Server, [stash_pid])]
    supervise child_processes, strategy: :one_for_one
  end
end

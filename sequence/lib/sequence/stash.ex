defmodule Sequence.Stash do
  use GenServer

  ############################################################
  ## Client Api
  ############################################################
  def start_link(current_number) do
    {:ok, pid} = GenServer.start_link(__MODULE__, current_number, [name: __MODULE__])
  end

  def save_value(value), do: GenServer.cast(__MODULE__, {:save_value, value})

  def save_value(pid, value) do
    GenServer.cast pid, {:save_value, value}
  end

  def get_value(pid \\ __MODULE__) do
    GenServer.call pid, :get_value
  end
  
  ############################################################
  ## Server Api
  ############################################################

  def handle_call(:get_value, _from, current_value) do
    {:reply, current_value, current_value}
  end

  def handle_cast({:save_value, value}, _current_value) when is_number(value) do
    {:noreply, value}
  end

  def handle_cast({:save_value, value}, current_value) do
    IO.puts "Not updating #{current_value} to #{value}, invalid input, must be a number"
    {:noreply, current_value}
  end
end

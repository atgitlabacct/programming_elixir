defmodule Sequence.Server do
  use GenServer
  @vsn "0"


  ############################################################
  ## Client Api
  ############################################################
  def start_link(stash_pid, opts \\ [name: __MODULE__]) do
    # This should return {:ok, pid} for supervisors.
    # If it is not returned then Supervisor.start_link will fail
    {:ok, pid} = GenServer.start_link(__MODULE__, stash_pid, opts)
  end

  def status(pid \\ __MODULE__) do
    GenServer.call(pid, :status)
  end

  def increment(pid \\ __MODULE__, delta) do
    GenServer.cast(pid, {:increment, delta})
  end

  def set_number(new_number, pid \\ __MODULE__) do
    GenServer.call(pid, {:set_number, new_number})
  end

  def next_number(pid \\ __MODULE__), do: GenServer.call(pid, :next_number)

  ############################################################
  ## Server Api
  ############################################################
  def init(stash_pid) do
    {{year, month, day}, {hour, min, sec}} = :erlang.localtime()
    IO.puts "Server started at #{year}-#{month}-#{day}, #{hour}:#{min}:#{sec}"
    current_number = Sequence.Stash.get_value stash_pid
    {:ok, {current_number, stash_pid}}
  end

  def handle_call(:next_number, _from, {current_number, stash_pid}) do
    {:reply, current_number, {current_number + 1, stash_pid}}
  end

  def handle_call({:set_number, new_number}, _from, {_current_number, stash_pid}) do
    {:reply, new_number, {new_number, stash_pid}}
  end

  def handle_call(:status, _from, {current_number, stash_pid}) do
    {:reply, current_number, {current_number, stash_pid}}
  end

  def handle_cast({:increment, delta}, {current_number, stash_pid}) do
    {:noreply, {current_number + delta, stash_pid}}
  end

  def handle_info(msg, state) do
    IO.puts "INFO: #{IO.inspect msg}"
    {:noreply, state}
  end

  def format_status(_reason, [_pdict, state]) do
    [data: [{'State', "My current state is '#{inspect state}', and I'm happy"}]]
  end

  def terminate(_reason, {current_number, stash_pid}) do
    Sequence.Stash.save_value stash_pid, current_number
  end
end

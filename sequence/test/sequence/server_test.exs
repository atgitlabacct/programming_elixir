defmodule Sequence.ServerTest do
  use ExUnit.Case

  test "returns the current state" do
    {:ok, pid} = Sequence.Stash.start_link(50)
    Sequence.Server.start_link(pid)
    assert 50 = Sequence.Server.status
  end

  test "sets a new number" do
    {:ok, pid} = Sequence.Stash.start_link(50)
    Sequence.Server.start_link(pid)
    Sequence.Server.set_number(22)
    assert 22 = Sequence.Server.status
  end

  test "increments a number" do
    {:ok, pid} = Sequence.Stash.start_link(50)
    Sequence.Server.start_link(pid)
    Sequence.Server.increment(23)
    assert 73 = Sequence.Server.status
  end
end

defmodule SequenceTest do
  use ExUnit.Case

  test "applies the delta repeatedly" do
    Sequence.Supervisor.start_link 500

    Sequence.Server.next_number
    Sequence.Server.increment 10
    assert Sequence.Server.next_number == 511
  end
end

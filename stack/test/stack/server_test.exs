defmodule Stack.ServerTest do
  use ExUnit.Case, async: false

  setup do
    stack = [1001, "foo"]
    {:ok, stash_pid} = GenServer.start(Stack.Stash, stack)
    {:ok, stack_pid } = GenServer.start(Stack.Server, stash_pid)
    {:ok, stash_pid: stash_pid, stack_pid: stack_pid, stack: stack}
  end

  test "on initialization it reads from the a stash", context do
    assert stack = Stack.Server.status(context[:stack_pid])
  end

  test "we return the head of stack", context do
    assert 1001 = Stack.Server.pop context[:stack_pid]
    assert "foo" = Stack.Server.pop context[:stack_pid]
  end

  test "push a new value onto the stack", context do
    Stack.Server.push(context[:stack_pid], "baz")
    assert ["baz", 1001, "foo"] = Stack.Server.status(context[:stack_pid]).current_stack
  end

  test "on termination it writes the stack to the stash", context do
    Stack.Server.push(context[:stack_pid], "baz")
    Stack.Server.stop(context[:stack_pid])

    assert ["baz", 1001, "foo"] = Stack.Stash.get(context[:stash_pid])
  end
end

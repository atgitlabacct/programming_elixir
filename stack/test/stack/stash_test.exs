defmodule Stack.StashTest do
  use ExUnit.Case

  alias Stack.Stash, as: SS

  setup do
    {:ok, pid} = SS.start_link([10])
    {:ok, stash_pid: pid}
  end

  test "registers the process with the module name" do
    assert Enum.member?(Process.registered, :Stack.Stash)
  end

  test "initializes a value", context do
    assert [10] = SS.get context[:stash_pid]
  end

  test "sets a new stash value", context do
    spid = context[:stash_pid]
    SS.save(spid, ["foo", "bar"])
    assert ["foo", "bar"] = SS.get spid
  end

  test "does not save unless list", context do
    spid = context[:stash_pid]
    SS.save(spid, "foo")
    assert [10] = SS.get spid 
    SS.save(spid, :foo)
    assert [10] = SS.get spid 
  end
end

defmodule Stack do
  use Application

  def start(_type, _args) do
    Stack.Supervisor.start_link
  end
end

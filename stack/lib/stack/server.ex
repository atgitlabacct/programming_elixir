defmodule Stack.Server do
  use GenServer

  ############################################################
  ## Client Api
  ############################################################
  def start_link(stash_pid, opts \\ [name: __MODULE__]) do
    GenServer.start_link(__MODULE__, stash_pid, opts)
  end

  def pop(pid) do
    GenServer.call(pid, :pop)
  end

  def push(pid, value) do
    GenServer.cast(pid, {:push, value})
  end

  def status(pid) do
    GenServer.call(pid, :status)
  end

  def raise_error(pid) do
    GenServer.cast(pid, :raise_error)
  end

  def stop(pid) do
    GenServer.call(pid, :stop)
  end

  ############################################################
  ## Server Api
  ############################################################

  def init(stash_pid) do
    {:ok, %{stash_pid: stash_pid, current_stack: Stack.Stash.get(stash_pid)}}
  end

  def handle_call(:pop, _from, state) do
    [h|tail] = state.current_stack 
    {:reply, h, %{state | current_stack: tail}}
  end

  def handle_call(:status, _from, state) do
    {:reply, state, state}
  end

  def handle_call(:stop, _from, state) do
    {:stop, :normal, {:ok, state}, state}
  end

  def handle_cast({:push, v}, state) do
    new_stack = [v | state.current_stack]
    {:noreply, %{state | current_stack: new_stack}}
  end

  def handle_cast(:error, state) do
    :foo + 100
    {:noreply, state}
  end

  def terminate(_reason, state) do
    IO.puts "Terminating" 
    Stack.Stash.save(state.stash_pid, state.current_stack)
    :ok
  end
end

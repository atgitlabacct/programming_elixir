defmodule Stack.Stash do
  use GenServer

  ############################################################
  ## Client Api
  ############################################################

  def start_link(val, opts \\ [name: __MODULE__]) do
    GenServer.start_link(__MODULE__, val, opts)
  end

  def save(pid, value) do
    GenServer.cast(pid, {:save, value})
  end

  def get(pid) do
    GenServer.call(pid, :get)
  end

  ############################################################
  ## Server Api
  ############################################################

  def init(state) when is_list(state) do
    {:ok, state}
  end
  
  def handle_cast({:save, val}, _state) when is_list(val) do
    {:noreply, val}
  end

  def handle_cast({:save, _val}, state) do
    {:noreply, state}
  end


  def handle_call(:get, _from, state) do
    {:reply, state, state}
  end

end

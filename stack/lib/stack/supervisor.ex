defmodule Stack.Supervisor do
  use Supervisor

  def start_link do
    result = {:ok, main_sup_pid} = Supervisor.start_link(__MODULE__, [], 
    [strategy: :one_for_one, 
      name: __MODULE__])
    start_workers(main_sup_pid)
    result
  end

  # OTP needs to have an init to setup the supervise true
  def init(_), do: supervise([], strategy: :one_for_one)

  defp start_workers(main_sup_pid) do
    # Start the stash
    initial_stash = Application.get_env(:stack, :initial_stash)
    {:ok, stash_pid} = Supervisor.start_child(main_sup_pid, 
                                              worker(Stack.Stash, [ initial_stash ]))

    # Start the subsupervisor and give it the stash pid
    Supervisor.start_child(main_sup_pid, worker(Stack.SubSupervisor, [ stash_pid ]))
  end
end

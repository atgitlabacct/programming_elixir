# This is suboptimal because each call
# takes a collection and returns a collection
IO.puts File.read!("/usr/share/dict/words")
  |> String.split
  |> Enum.max_by(&String.length/1)

l = Enum.to_list(1..100)
  |> Stream.map(&(&1 * &1))
  |> Stream.map(&(&1 + 1))
  |> Stream.filter(fn x -> rem(x,2) == 1 end)
  |> Enum.join(",")

IO.puts(l)

ExUnit.start()

defmodule WorkingWithMultipleProcessesTest do

  defmodule CatFinderTest do
    use ExUnit.Case, async: true
    
    test "find cats string in a file" do
      WorkingWithMultipleProcesses.CatFinder.find("/Users/adam/test/test1.txt", self)
      assert_receive {:result, {"test1.txt", 1}, self}
    end
  end

end

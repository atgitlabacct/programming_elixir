ExUnit.start()

defmodule Exercises do
  defmodule Nodes_4 do
    defmodule TickerTests do
      use ExUnit.Case

      test "asks a handler to find the next pid to tick" do
        :global.register_name(:ringhandler, self)
        ticker = Exercises.Nodes_4.Ticker.start
        Exercises.Nodes_4.Ticker.tick(ticker)
        assert_receive {^ticker, :pid_to_tick}, 3000
      end
    end
  end
end

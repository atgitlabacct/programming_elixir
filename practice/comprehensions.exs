defmodule Practice do
  def run do
    a = [ %{id: 10, state: "WI", cost: 10},
          %{id: 11, state: "MI", cost: 20},
          %{id: 11, state: "WA", cost: 12} 
        ]

    b = [ 
          %{ state: "WI", rate: 0.05 } 
        ]

    for %{id: id, state: st, cost: cost} <- a,
        %{state: st_tax, rate: rate} <- b,
        st == st_tax,
    do: %{id: id, st: st, cost: cost, rate: rate, total: (cost * (rate + 1))}
  end
end

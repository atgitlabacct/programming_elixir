defmodule Banking do
  def simple_interest(balance, 0), do: balance 
  def simple_interest(0, _rate), do: 0 
  def simple_interest(balance, rate) do
    interest(balance, rate)
  end

  def annual_interest(balance, 0, _years), do: balance
  def annual_interest(0, _rate, _years), do: 0
  def annual_interest(balance, _rate, 0), do: balance
  def annual_interest(balance, rate, years) do
    payment = balance / years
    annual_interest_minus_payment(balance, rate, payment, years)
  end

  defp annual_interest_minus_payment(balance, rate, _payment, 0) do
    IO.puts "Paid off"
  end

  defp annual_interest_minus_payment(balance, rate, payment, years) when years > 0 do
    IO.puts "Year: #{years} -- Balance: #{balance} -- Interest: #{interest(balance, rate)}"
    annual_interest_minus_payment(balance-payment, rate, payment, years - 1)
  end

  defp interest(balance, rate) do
    balance * rate
  end
end

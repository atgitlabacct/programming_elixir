defmodule Tasks2.Fib do
  def of(0), do: 0
  def of(1), do: 1
  def of(n), do: __MODULE__.of(n - 1) + __MODULE__.of(n - 2)
end

worker = Task.async(Tasks2.Fib, :of, [20])
result = Task.await(worker)
IO.puts "The result is #{result}"

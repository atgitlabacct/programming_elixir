defmodule Ticker do
  @interval 2000
  @name :ticker

  def start do
    pid = spawn(__MODULE__, :generator, [[], []])
    :global.register_name(@name, pid)
  end

  def register(client_pid) do
    send :global.whereis_name(@name), {:register, client_pid }
  end

  def shutdown, do: send(:global.whereis_name(@name), {self, :shutdown})

  def generator(clients, next_clients) do
    receive do
      {:register, pid} ->
        IO.puts "registering #{inspect pid}"
        generator([pid | clients], [pid | next_clients])

      {_from, :shutdown} ->
      Enum.each(clients, fn(client) ->
        IO.puts "shutting down #{inspect client}"
        send(client, {self, :shutdown})
      end)
    after
      @interval ->
        # What to do if clients are empty.
        # I could solve by doing a diff way.
        # using the location List.at
        if Enum.empty?(next_clients) do
          send_tick(h)
          generator(clients, clients)
        else
          [h | t] = next_clients
          send_tick(h)
          generator(clients, t)
        end
    end
  end

  defp send_tick(client) do
    IO.puts "tick"
    IO.puts "sending to #{inspect client}"
    send client, {:tick}
  end

end

defmodule Client do
  def start do
    pid = spawn(__MODULE__, :receiver, [])
    Ticker.register(pid)
  end

  def receiver do
    receive do
      {:tick} ->
        IO.puts "tock in client"
        receiver
      {_from, :shutdown} ->
        IO.puts "#{inspect self} shutting down"
    end
  end
end

defmodule My do
  defmacro macro(code) do
    IO.inspect code
    quote do: IO.puts "Different code"
  end

  defmacro macro1(code) do
    quote do
      IO.inspect(unquote(code))
    end
  end
end

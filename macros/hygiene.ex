defmodule Scope do
  defmacro update_local(val) do
    local = "some value"
    result = quote do
      local = unquote(val)
      IO.puts "end of macro body, local = #{local}"
    end

    IO.puts "In macro definition, local = #{local}"
    result
  end
end

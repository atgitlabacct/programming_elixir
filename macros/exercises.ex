defmodule Exercises do
  defmacro unless(condition, clauses) do
    do_clause = Keyword.get(clauses, :do, nil)
    else_clause = Keyword.get(clauses, :else, nil)

    quote do
      if unquote(condition),
        do: unquote(else_clause),
      else: unquote(do_clause)
    end
  end

  defmacro times_n(n) do
    quote do
      def unquote(:"times_#{n}")(a) do
        unquote(n) * a
      end
    end
  end
end

defmodule ExTest do
  require Exercises

  def unquote(:test)() do
    "test"
  end
  Exercises.times_n(3)
  Exercises.times_n(4)

end

defmodule ProgElixir.Macros.Fooling do
  defmacro some_method(code) do
    IO.puts inspect(code) # Evaluated code prints internal tuple
    quote do # Start parsing into internal tuple, not evaluated
      IO.puts "Hello Macros" # Parsed - Converted to tuple fragment
      unquote(code) # Not parsed, "code" is injected at this point
      IO.puts "Goodbye Macros" # Parsed - Converted to tuple fragment
    end
  end
end

defmodule ProgElixir.Macros.FoolingTest do
  require ProgElixir.Macros.Fooling
  alias ProgElixir.Macros.Fooling

  def test(bool) do
    if bool do
      Fooling.some_method(IO.puts("Middle child"))
    else
      IO.puts "Macro was parsed, but not executed"
    end
  end
end

defmodule Exercises do
  defmodule StringsAndBinaries do
    defmodule One do
      def printable_chars([]), do: true 
      def printable_chars([h|t])
      when h in (32..127) do
        printable_chars(t)
      end
      def printable_chars(_), do: false
    end

    defmodule Two do
      def multiple_anagrams(w1, wlist, acc \\ [])
      def multiple_anagrams(_w1, [], acc), do: Enum.reverse(acc)
      def multiple_anagrams(w1, [h | t], acc) do
        if anagram?(w1, h) do
          multiple_anagrams(w1, t, [h | acc])
        else
          multiple_anagrams(w1, t, acc)
        end
      end

      def anagram?(w1, w2), do: _anagram?(to_char_list(w1), to_char_list(w2))
      defp _anagram?([], []), do: true
      defp _anagram?([], _), do: false 
      defp _anagram?(_, []), do: false 
      defp _anagram?([h | t], w2) 
      when is_list(w2) do
        if h in w2 do
          anagram?(t, w2 -- [ h ])
        else
          false
        end
      end

    end
  end
end

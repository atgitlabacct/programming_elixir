defmodule ProcessToken do
  def run do
    spawn(ProcessToken, :process, [])
  end

  def send_token(pid, to, token), do: send(pid, {to, token})

  def stop(pid), do: send(pid, :stop)

  def process do
    receive do
      {to, "betty"} ->
        IO.puts "Received token betty resending"
        send to, "betty"
        process
      {to, "fred"} ->
        IO.puts "Received token fred resending"
        send to, "fred"
        process
      "betty" ->
        IO.puts "Received betty"
        process
      "fred" ->
        IO.puts "Received fred"
        process
      :stop ->
        IO.puts "stopping process"
        IO.inspect self
      _ ->
        IO.puts "Can't process. stopping"
    end
  end
end



p1 = ProcessToken.run
p2 = ProcessToken.run

send p2, {p1, "betty"}
send p1, {p2, "fred"}
send p2, :stop
send p1, :stop

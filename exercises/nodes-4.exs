defmodule Exercises do
  defmodule Nodes_4 do
    defmodule RingHandler do
      @name :ringhandler
      ############################################################
      ## Public Api
      ############################################################
      def start() do
        pid = spawn(__MODULE__, :process, [[], nil])
        :global.register_name(@name, pid)
      end

      def bootstrap do
        start
        p1 = Exercises.Nodes_4.Ticker.start
        p2 = Exercises.Nodes_4.Ticker.start
        p3 = Exercises.Nodes_4.Ticker.start
        add(p1)
        add(p2)
        add(p3)
        [p1,p2,p3]
        first_tick
      end

      def first_tick do
        send :global.whereis_name(@name), {self, :first_tick}
      end

      def list_ring do
        send :global.whereis_name(@name), {self, :list_ring}
      end

      def find_pid_to_tick do
        send :global.whereis_name(@name), {self, :pid_to_tick}
      end

      def remove(pid) do
        send :global.whereis_name(@name), {self, :remove, pid}
      end

      def add(pid) do
        send :global.whereis_name(@name), {self, :add, pid}
      end

      def shutdown, do: send(:global.whereis_name(@name), {self, :shutdown})

      ############################################################
      ## Private Api
      ############################################################
      def process(pid_ring, pid_to_tick) do
        receive do
          {from, :pid_to_tick} ->
            pid_to_tick = find_pid(from, pid_ring)
            IO.puts "finding pid to tick #{inspect pid_to_tick}"
            send from, {self, :pid_to_tick, pid_to_tick}

          {_from, :first_tick} ->
            IO.puts "Sending to first pid #{inspect List.first(pid_ring)}"
            send List.first(pid_ring), {self, :tick}

          {_from, :list_ring} ->
            IO.puts "Current ring #{inspect pid_ring}"

          {_from, :add, pid} ->
            if Enum.empty?(pid_ring) do
              pid_ring = [pid]
            else
              pid_ring = Enum.concat(pid_ring, [pid])
            end

          {_form, :remove, pid} when (pid == pid_to_tick) ->
            :timer.sleep(100)
            send self, {self, :remove, pid}

          {_form, :remove, pid} ->
            pid_ring = List.delete(pid_ring, pid)
            IO.puts "Removing #{inspect pid}"
            send pid, {self, :shutdown}

          {_from, :shutdown} when length(pid_ring) == 0 ->
            IO.puts "Shutting down handler"
            exit(:normal)

          {_from, :shutdown} ->
            Enum.map pid_ring, fn(pid) -> send(self, {self, :remove, pid}) end
            send self, {self, :shutdown}
        end

        process(pid_ring, pid_to_tick)
      end

      defp find_pid(from, pid_ring) when length(pid_ring) > 1 do
        idx = Enum.find_index(pid_ring, &(&1 == from))
        if idx >= (length(pid_ring) - 1) do
          {:ok, pid} = Enum.fetch(pid_ring, 0)
        else
          {:ok, pid} = Enum.fetch(pid_ring, idx + 1)
        end
        pid
      end

      defp find_pid(_from, pid_ring), do: nil
    end

  end
end

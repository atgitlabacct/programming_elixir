defmodule ListsAndRecursion do
  def span(from, to) when from == to or from > to, do: [from]
  def span(from, to) when from < to, do: [ from | span(from+1, to) ]

  # First attempt
  # Lets find all the primes.
  def find_primes(n) do
    for x <- span(2, n), is_prime?(x), do: x
  end

  defp is_prime?(n, d \\ 2)
  defp is_prime?(n, d) when n > d do
    if rem(n, d) == 0 do
      false
    else
      is_prime?(n, d + 1)
    end
  end
  defp is_prime?(_n, _divisor), do: true


  # Here lets just find all the multiples of the numbers in a range
  # so 2 * 2, 3, 4...n
  #    3 * 3, 4, 5...n
  #    ....
  # since we get all the multiples and we have a range...lets simply remove
  # the "non-prime" numbers at this point
  def find_primes_exclude(n) do
    range = span(2, n)
    # Get multiples and remove them from range
    range -- (for a <- range, b <- range, a <= b, a*b <= n, do: a * b)
  end
end

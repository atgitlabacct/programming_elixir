defmodule WorkingWithMultipleProcesses do
  import :timer, only: [ sleep: 1 ]

  defmodule Parent do
    def start do
      spawn_link(WorkingWithMultipleProcesses.Child, :run, [self])
      run
    end

    def run do
    sleep 500
    receive do
        msg ->
          IO.puts "Message is #{inspect msg}"
          run
      end
    end
  end

  defmodule Child do

    def run(parent_pid) do
      send parent_pid, :foo
      raise("boom")
    end
    
  end
end


fizz_buzz = fn
  (0,0, _) -> IO.puts "FizzBuzz"
  (_,0,_) -> IO.puts "Buzz"
  (_,_,c) -> IO.puts "#{c}"
end

fizz_buzz.(0,0,10)
fizz_buzz.(1,0,10)
fizz_buzz.(1,1,10)


defmodule WorkingWithMultipleProcesses do
  def pmap(coll, fun) do
    me = self
    coll
    |> Enum.map( fn(elem) ->
      spawn_link(fn -> (send me, { self, fun.(elem) }) end)
    end)
    |> Enum.map(fn (pid) ->
      receive do { ^pid, result } -> result end
      # Without the pin operator the results are ordered incorrectly
      #receive do { pid, result } -> result end
    end)
  end
end

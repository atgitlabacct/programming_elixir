defmodule WorkingWithMultipleProcesses do
  defmodule CatFinder do
    def find(file, sender) do
      {:ok, data} = File.read(file)
      result = find_cats(data)

      send sender, {:result, { Path.basename(file), result }, self}
    end

    defp find_cats(data) do
      Enum.count Regex.scan(~r/cat/i, data)
    end
  end

  defmodule Runner do
    def find_cats(dir) do
      pids = File.ls!(dir)
      |> Enum.map(fn(file) -> 
          spawn_link(WorkingWithMultipleProcesses.CatFinder, 
                      :find, 
                      [Path.join(dir, file), self]) 
         end)

      process(pids, [])
    end

    defp process(queue, results) do
      receive do
        {:result, msg, sender} when length(queue) > 1 ->
          process(List.delete(queue, sender), [msg | results])

        {:result, msg, _sender} ->
          [msg | results]
      end
    end
  end
end


#directory = "/Users/adam/test"
#IO.puts "cat in files #{inspect WorkingWithMultipleProcesses.Runner.find_cats(directory)}"


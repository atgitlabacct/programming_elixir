defmodule Exercises do
  defmodule Nodes_4 do
    defmodule Ticker do
      @handler :ringhandler
      ############################################################
      ## Public API
      ############################################################
      def start do
        handler = :global.whereis_name(@handler)
        if(handler == :undefined, do: raise("Ring Handler not found"))
        spawn(__MODULE__, :process, [handler])
      end

      def tick(pid), do: send(pid, {self, :tick})

      def tick_next_pid(ticker, pid) do
        send(ticker, {self, :pid_to_tick, pid})
      end

      ############################################################
      ## Private api
      ############################################################
      def process(ring_handler) do
        receive do
          {from, :tick} ->
            IO.puts "Tick from #{inspect from} -- Tock #{inspect self}, sleep for 2s"
            :timer.sleep(2000)
            Exercises.Nodes_4.RingHandler.find_pid_to_tick

          {_from, :pid_to_tick, pid} when is_nil(pid) ->
            IO.puts "#{inspect self} has nothing tick"

          {_from, :pid_to_tick, pid} ->
            tick(pid)

          {_from, :shutdown} ->
            IO.puts "Shutting down #{inspect self}"
            exit(:normal)
        end
        process(ring_handler)
      end
    end
  end
end

defmodule WorkingWithMultipleProcesses do
  import :timer, only: [ sleep: 1 ]

  # Whats interesting is spawn_link will immedietly die when the child
  # exits.  spawn_monitor will actually recieve the messages of :foo and
  # the DOWN mesage on an 'exit("boom")'
  #
  # A similiar thing happens when issuing a 'raise' function.
  # We get the error first but the messages still appear.
  defmodule Parent do
    def start do
      spawn_monitor(WorkingWithMultipleProcesses.Child, :run, [self])
      run
    end

    def run do
    sleep 500
    receive do
        msg ->
          IO.puts "Message is #{inspect msg}"
          run
      end
    end
  end

  defmodule Child do

    def run(parent_pid) do
      send parent_pid, :foo
      #exit("boom")
      raise ("boom")
    end
    
  end
end

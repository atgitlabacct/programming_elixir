
fizz_buzz = fn
  (0,0, _) -> IO.puts "fizzbuzz"
  (_,0,_) -> IO.puts "buzz"
  (_,_,c) -> IO.puts "#{c}"
end


 f1 = fn(n) ->
   fizz_buzz(rem(n,3), rem(n, 5), n)
 end

f1(10)

Enum.map [1,2,3,4], fn x -> IO.puts(x + 2) end
Enum.each [1,2,3,4], fn x -> IO.inspect x end

IO.puts "Function shortcut"

Enum.map [1,2,3,4], &(IO.puts(&1 +2))
IO.puts "========"
Enum.each [1,2,3,4], &(IO.inspect&1)
IO.puts "========"
Enum.each [1,2,3,4], &IO.inspect/1

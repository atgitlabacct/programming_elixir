defmodule MyEnum do
  def each([], _fnc), do: []
  def each(l, fnc), do: each(l, fnc, [])

  defp each([], _fnc, result), do: Enum.reverse(result)
  defp each([head|tail], fnc, result) do
      each(tail, fnc, [fnc.(head) | result])
  end


  def any?([], _fnc), do: false
  def any?([head | tail], fnc), do: any?(tail, fnc, fnc.(head))

  defp any?(_list, _fnc, false), do: false
  defp any?([], _fnc, true), do: true
  defp any?([head | tail], fnc, true), do: any?(tail, fnc, fnc.(head))


  def split([], _pos), do: []
  def split(dict, pos), do: split(dict, pos, 0, [])

  defp split([], _pos, _count, new_list), do: {Enum.reverse(new_list), []}
  defp split([head | tail], pos, count, new_list) when (count < pos) do
    split(tail, pos, count + 1, [head | new_list])
  end
  defp split(tail, _pos, _count, new_list), do: {Enum.reverse(new_list), tail}


  def take([], _num), do: []
  def take(dict, num), do: take(dict, num, 0, [])
  defp take([], _num, _count, acc), do: Enum.reverse(acc)
  defp take([h|t], num, count, acc) when count < num do
    take(t, num, count + 1, [h | acc])
  end
  defp take(_dict, _num, _count, acc), do: Enum.reverse(acc)

  def take2(dict, num) do
    {f, _} = split(dict, num)
    f
  end

  def filter([], _f), do: []
  def filter(dict, f), do: filter(dict, f, [])
  defp filter([], _f, acc), do: Enum.reverse(acc)
  defp filter([h|t], f, acc) do
    if f.(h) do
      filter(t, f, [h | acc])
    else
      filter(t, f, acc)
    end
  end

  def flatten(coll, acc \\ [])
  def flatten([], acc), do: Enum.reverse(acc)
  def flatten([h | t], acc) when is_list(h), do: flatten(h, acc)
  def flatten([h | t], acc), do: flatten(t, [ h| acc])

end

defmodule Chop do
  # 10, (1..100)
  def guess(actual, range) do
    my_guess = mid_point_val(range)
    find_guess(actual, my_guess, range)
  end

  defp find_guess(actual, my_guess, range) when my_guess == actual, do: IO.puts "Its #{my_guess}"
  defp find_guess(actual, my_guess, range) when my_guess < actual do
    IO.puts "Is it #{my_guess}"
    min = mid_point_val(range) + 1
    max = Enum.max(range)
    new_range = min..max
    find_guess(actual, mid_point_val(new_range), new_range)
  end

  defp find_guess(actual, my_guess, range) when my_guess > actual do
    IO.puts "Is it #{my_guess}"
    min = Enum.min(range)
    max = mid_point_val(range) - 1
    new_range = min..max
    find_guess(actual, mid_point_val(new_range), new_range)
  end

  defp mid_point_val(range) do
    Enum.at(range, mid_point(range) - 1)
  end

  defp mid_point(range) do
    div(Enum.count(range), 2)
  end
end

